# laravel-api-kubernetes



## Introducción

Repositorio para la prueba de integración de una API de Laravel con Kubernetes. Procediendo posteriormente a un proceso de CI/CD.

## Instalación

1.  Se clona el repositorio.

2.  Se agrega la base de datos.

```bash
cd docker
docker-compose up -d
```

composer require laravel/jetstream
php artisan jetstream:install livewire
npm install
npm install process
npm run dev